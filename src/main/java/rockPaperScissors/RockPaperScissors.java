package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {

    	String yesOrNo = "y";
    	String humanRPS = "";
    	String compRPS = "";
    	int randomNumber = 0;
    	Random rand = new Random();
    	
    	while (yesOrNo.equals("y")) {
    		System.out.println("Let's play round " + roundCounter);
    		randomNumber = rand.nextInt(3);
    		compRPS = rpsChoices.get(randomNumber);
    		
    		humanRPS = readInput("Your choice (Rock/Paper/Scissors)?");
    		while (!rpsChoices.contains(humanRPS)) {
    			System.out.println("I do not understand " + humanRPS + ". Could you try again?");
    			humanRPS = readInput("Your choice (Rock/Paper/Scissors)?");
    		}
    		
    		if (humanRPS.equals(compRPS)) {
    			System.out.println("Human chose " + humanRPS + ", computer chose " + compRPS + ". It's a tie!");
    		}  
    		else if ((humanRPS.equals("rock") && compRPS.equals("paper")) ||
    				(humanRPS.equals("paper") && compRPS.equals("scissors")) ||
    				(humanRPS.equals("scissors") && compRPS.equals("rock"))) {
    				System.out.println("Human chose " + humanRPS + ", computer chose " + compRPS + ". Computer wins!");
    				computerScore++;
    		}  
    		else {
    			System.out.println("Human chose " + humanRPS + ", computer chose " + compRPS + ". Human wins!");
    	    	humanScore++;
    	    }
    		    		
    		System.out.println("Score: human " + humanScore + ", computer " + computerScore);
    		roundCounter++;
    		
    		yesOrNo = readInput("Do you wish to continue playing? (y/n)?");
    		while (!(yesOrNo.equals("y") || yesOrNo.equals("n"))) {
    			System.out.println("I do not understand " + yesOrNo + ". Could you try again?");
    			yesOrNo = readInput("Do you wish to continue playing? (y/n)?");
    			
    		}
    		
    		if (yesOrNo.equals("n")) {
				System.out.println("Bye bye :)");
			}  		
    	}	
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}